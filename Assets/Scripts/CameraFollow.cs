﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CameraFollow : NetworkBehaviour {

	public Transform target; //what to follow

	Vector3 offset;
	private float topOffset = 1.5f;
	private float smoothing = 30f; //camera speed

	void Start()
	{
		offset = transform.position - target.position;
	}

	void FixedUpdate()
	{
		if (target) {
			Vector3 targetCamPos = new Vector3 (transform.position.x, target.position.y + topOffset, offset.z);

			transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
		}
	}
}