﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class StartGameLine : NetworkBehaviour {

	private Renderer rend; 
	public Color stopColor = Color.red;
	public Color startColor = Color.green;

	void Start () {
		rend = GetComponent<Renderer>();
		rend.material.color = stopColor;
	}

	public void Go () {
		rend.material.color = startColor;
	}

	public void Wait () {
		rend.material.color = stopColor;
	}

}
