﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameController : NetworkBehaviour {

	public static GameController instance;
	public StartGameLine StartGameLine;
	public HUD HUD;

	public bool gameOn;
	public bool raceOver;
	public bool onePlayerGame = true; // For development

	private int playerCount = 0;
	private float delayStart = 3.7f;

	private AudioSource[] audioSources;
	AudioSource startMotorAudio;
	AudioSource startGameAudio;

	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

		// Audio
		audioSources = GetComponents<AudioSource>();
		startGameAudio = audioSources [0];
	}

	void Start () {
		// If one player game
		if (onePlayerGame)
			delayStart = 0f;
	}
	
	void Update () {
		
	}

	[ClientRpc]
	public void RpcStartGame() {
		// Play start sound
		startGameAudio.Play();

		StartCoroutine (StartGameAfterSound ());
	}

	IEnumerator StartGameAfterSound() {
		// Wait for audio to be finished
		yield return new WaitForSeconds(delayStart);

		// Tell all players to start their engines
		GameOn ();
	}

	public void GameOn () {
		gameOn = true;

		// Show that the players can start
		StartGameLine.Go ();

		// Tell HUD that the game started
		HUD.GameStarted ();
	}
		
	public void RaceOver () {
		raceOver = true;
	}

	public void PlayerCount (int playerCountUpdate) {
		playerCount = playerCountUpdate;

		// Tell HUD that playerCount changed
		HUD.PlayerCount(playerCountUpdate);
	}

	public void Winner (string netId) {
		// Only one player can win
		if (raceOver) {
			return;
		} else {
			raceOver = true;
		}

		// Tell players "loser" or "winner"
		GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");

		foreach (GameObject Player in Players)
		{
			NetworkIdentity PlayerIdentity = Player.GetComponent<NetworkIdentity> ();
			Player PlayerScript = Player.GetComponent<Player> ();

			if (PlayerIdentity.netId.ToString() == netId) { 
				PlayerScript.YouWin ();
			} else {
				PlayerScript.YouLose ();
			}
		}
	}
}
