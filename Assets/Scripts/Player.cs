﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Player : NetworkBehaviour {

	public GameObject CharacterPrefab;

	public AudioClip hitAudio;
	public AudioClip winAudio;
	public AudioClip loseAudio;

	private GameObject Character;
	private Character CharacterScript;

	[SyncVar]
	public bool loser = false;
	[SyncVar]
	public bool winner = false;

	private float _speed = 25f;
	private int previousRock = 0;

	private AudioSource[] audioSources;
	AudioSource startMotorAudio;
	AudioSource motorAudio;

	void Awake() {
		audioSources = GetComponents<AudioSource>();
		startMotorAudio = audioSources [1];
		motorAudio = audioSources [2];
	}

	void Start () {
		if (isLocalPlayer == false) {
			return;
		}

		// Server work
		CmdSpawnCharacter();
		CmdPlayersReady ();
	}

	void Update () {
		if (isLocalPlayer == false) {
			return;
		}

		bool gameOn = GameController.instance.gameOn;

		// Start game (press space)
		if (Input.GetKeyDown("space") && !gameOn) {
			CmdStartGame();
		}

		// Restart (R)
		if( Input.GetKeyDown(KeyCode.R) )
		{
			SceneManager.LoadScene( SceneManager.GetActiveScene().name );	
			CmdStopHost (); 
		}
			
		// Start motor
		if (Input.GetButtonDown ("Accelerate") && gameOn) {
			motorAudio.Play ();
			startMotorAudio.Play ();
		}

		// Stop motor
		if (Input.GetButtonUp ("Accelerate") && gameOn) {
			startMotorAudio.Stop ();
			motorAudio.Stop ();
		}
	}

	void FixedUpdate () {
		if (isLocalPlayer == false) {
			return;
		}

		Rigidbody RigidBody = GetComponent<Rigidbody> ();

		// Check if game started
		bool gameOn = GameController.instance.gameOn;

		// Forward movement
		if (Input.GetButton ("Accelerate") && gameOn) {
			RigidBody.AddForce (transform.up * _speed);
		}

		// Rotate back to forward position
		if (RigidBody.angularVelocity.z != 0 && RigidBody.angularVelocity.z < 1 && RigidBody.angularVelocity.z > -1) {
			Quaternion desiredRotQ = Quaternion.Euler (transform.eulerAngles.x, transform.eulerAngles.y, 0);
			transform.rotation = Quaternion.Lerp (transform.rotation, desiredRotQ, Time.deltaTime * 2);
		} 

		// Turn
		RigidBody.AddForce (transform.right * Input.GetAxis("Horizontal") * _speed);

		// Let the camera follow this player
		Camera.main.GetComponent<CameraFollow>().target=transform; //Fix camera on "me"

		// When player exits the screen on left, appear on the right and visa versa
		KeepPlayerInTheScreen();
	}

	void KeepPlayerInTheScreen () {
		// TODO: Used Camera Viewport width instead of fixed values
		if (transform.position.x < -9.5f) {
			transform.position = new Vector3 (9.5f, transform.position.y, 0);
		} else if (transform.position.x > 9.5f) {
			transform.position = new Vector3 (-9.5f, transform.position.y, 0);
		}
	}
		
	public void YouLose () {
		loser = true;
		CmdLoserAmimation ();
		RpcLoseAudio ();
	}

	public void YouWin () {
		winner = true;
		CmdCelebrateAnimation ();
		RpcWinAudio ();
	}

	[ClientRpc]
	void RpcLoseAudio() {
		if (isLocalPlayer) {
			playClip (loseAudio);
		}
	}

	[ClientRpc]
	void RpcWinAudio() {
		if (isLocalPlayer) {
			playClip (winAudio);
		}
	}
		
	[Command]
	void CmdSpawnCharacter() {
		Character = Instantiate (CharacterPrefab, transform.position, Quaternion.identity);
		CharacterScript = Character.GetComponent<Character> ();
		NetworkServer.Spawn (Character);
	}

	// Send playerCount from server to client
	[Command]
	void CmdPlayersReady () {
		RpcPlayersReady (NetworkManager.singleton.numPlayers);
	}

	// Check if there are two players ready
	[ClientRpc]
	void RpcPlayersReady(int playerCount) {
		GameController.instance.PlayerCount (playerCount);
	}

	// Stop the clients for a restart
	[Command]
	void CmdStopHost(){
		NetworkManager.singleton.StopClient ();
		NetworkManager.singleton.StopHost ();
	}

	// Start game
	[Command]
	void CmdStartGame () {
		if (NetworkManager.singleton.numPlayers > 1 || GameController.instance.onePlayerGame) // Wait for at least two players
			GameController.instance.RpcStartGame ();
	}

	// Check collision with End of Game line
	private void OnTriggerEnter(Collider other)
	{
		if (isLocalPlayer == false) {
			return;
		}

		if (other.tag == "EndGameLine") {
			CmdPlayerFinished ();
		}
	}

	[Command]
	void CmdPlayerFinished () {
		GameController.instance.Winner (this.netId.ToString());
	}

	private void OnCollisionEnter(Collision other) {
		int instanceID = other.gameObject.GetInstanceID ();

		if (instanceID != previousRock) { // Buffer for hitting rocks TODO: Make more intelligent
			CmdHitRockAmimation ();
			playClip (hitAudio);
		}

		previousRock = instanceID;
	}

	[Command]
	void CmdCelebrateAnimation () {
		CharacterScript.RpcAnimateShot ();
	}

	[Command]
	void CmdLoserAmimation () {
		CharacterScript.RpcAnimateHit ();
	}

	[Command]
	void CmdHitRockAmimation () {
		CharacterScript.RpcAnimateFall ();
	}

	void playClip(AudioClip audioClip) {
		AudioSource audioSlot = audioSources [0];

		audioSlot.clip = audioClip;
		audioSlot.Play();
	}

}
