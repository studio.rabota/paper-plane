﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour {

	private BoxCollider2D groundCollider;
	private float groundHeight;
	private Vector3 cameraViewPort; 

	// Use this for initialization
	void Start () {
		groundHeight = transform.lossyScale.y;
	}
	
	// Update is called once per frame
	void Update () {
		cameraViewPort = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.nearClipPlane));

		if (cameraViewPort.y > (transform.position.y + (groundHeight / 2))) {
			RepositionBackground ();
		}
	}

	private void RepositionBackground() {
		float moveToY = transform.position.y + (groundHeight * 2);
		transform.position = new Vector3(transform.position.x, moveToY, transform.position.z); 
	}
}
