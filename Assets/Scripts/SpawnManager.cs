﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnManager : NetworkBehaviour {

	[SerializeField]
	private GameObject enemyShipPrefab;
	[SerializeField]
	private GameObject Boundary;
	[SerializeField]
	private GameObject StartGameLine;
	[SerializeField]
	private GameObject EndGameLine;

	public int numAsteroids = 30;

	private float boundaryStartY;
	private float boundaryEndY;
	private float boundaryWidth;
	private float spawnOffset = 2;
	private float spawnInterval = 0.6f;

	public override void OnStartServer() {
		if (!isServer) 
			return;

		boundaryStartY = StartGameLine.transform.position.y; 
		boundaryEndY = EndGameLine.transform.position.y; 
		boundaryWidth = Boundary.transform.lossyScale.x / 2;

		for(int i = 0; i < numAsteroids; i++)
		{
			spawnAsteroid ();
		}
	}

	void spawnAsteroid () {
		GameObject createAsteroid = Instantiate (enemyShipPrefab, new Vector3 (
			Random.Range (-boundaryWidth, boundaryWidth), 
			Random.Range (boundaryStartY, boundaryEndY), 
			0
		), Quaternion.identity);

		NetworkServer.Spawn(createAsteroid);
	}
}
