﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Character : NetworkBehaviour {

	private Animator _animator;
	public GameObject Alien;

	private bool animateHitBuffer = false;

	// Use this for initialization
	void Start () {

		// Get Alien
		_animator = Alien.GetComponent<Animator>();

	}

	[ClientRpc]
	public void RpcAnimateFall () {
		_animator.SetTrigger("fall");
	}

	[ClientRpc]
	public void RpcAnimateShot() {
		_animator.SetTrigger("shot");
	}

	[ClientRpc]
	public void RpcAnimatePoseGun () {
		_animator.SetTrigger("pose_gun");
	}

	[ClientRpc]
	public void RpcAnimateHit () {
		_animator.SetTrigger("hit");
	}
		
}
