﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	public GameObject MessagePanel;
	public string messageWaitPlayers = "Waiting for players"; 
	public string messageWait2ndPlayer = "Waiting for second player"; 
	public string messageHowToStartGame = "Press space to start the game"; 

	private int playerCount;

	// Use this for initialization
	void Start () {
		OpenMessagePanel (messageWaitPlayers);
	}
	
	// Update is called once per frame
	void Update () {}

	// React to game started 
	public void GameStarted () {
		CloseMessagePanel ();
	}

	// Player count
	public void PlayerCount (int playerCountServer) {
		playerCount = playerCountServer;

		if (playerCount == 1) {
			OpenMessagePanel (messageWait2ndPlayer);
		}

		if (playerCount == 2) {
			OpenMessagePanel (messageHowToStartGame);
		}
	}

	// Message panel
	private bool mIsMessagePanelOpened = false;

	public bool IsMessagePanelOpened
	{
		get { return mIsMessagePanelOpened; }
	}
 
	public void OpenMessagePanel(string text)
	{
		MessagePanel.SetActive(true);

		Text mpText = MessagePanel.transform.Find("Text").GetComponent<Text>();
		mpText.text = text;

		mIsMessagePanelOpened = true;
	}

	public void OpenAndCloseMessage(string text, float time)
	{
		StartCoroutine(OpenAndCloseMessageNumerator(text, time));
	}

	private IEnumerator OpenAndCloseMessageNumerator(string text, float time)
	{
		OpenMessagePanel (text);
		yield return new WaitForSeconds(time);

		Text mpText = MessagePanel.transform.Find("Text").GetComponent<Text>();
		if (mpText.text == text)
			CloseMessagePanel ();
	}

	public void CloseMessagePanel()
	{
		MessagePanel.SetActive(false);

		mIsMessagePanelOpened = false;
	}

}
