﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter2D (Collider2D other) {
		Debug.Log ("Hit");
		Destroy (other.gameObject);
		Destroy (gameObject);
	}

}
