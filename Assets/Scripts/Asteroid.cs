﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {
	[SerializeField]
	private GameObject _enemyExplosionPrefab;

	private float _speed = 0f;

	[SerializeField]
	private AudioClip _clip;

	void Start () {
		Rigidbody RigidBody = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void Update () {
		Rigidbody RigidBody = GetComponent<Rigidbody> ();
		RigidBody.AddForce (transform.up * _speed);
	}
}
