﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Alien : NetworkBehaviour {

	public AudioClip alienChatter;
	private bool isLeftPlayer;

	// Use this for initialization
	void Start () {
		// AudioSource.PlayClipAtPoint(jumpClip, transform.position);
		AudioSource.PlayClipAtPoint (alienChatter, Camera.main.transform.position, 1f);

		// Move a bit to the side of the plane
		isLeftPlayer = transform.position.x < 0;
		float movePosition = isLeftPlayer ? -2f : 2f;
		transform.Translate(new Vector3(movePosition, transform.position.y, transform.position.z));
	}

	private float smoothing = 30f; //camera speed

	void FixedUpdate()
	{
		Vector3 targetCamPos = new Vector3(transform.position.x, Camera.main.transform.position.y - 7, transform.position.z);

		transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
	}
}
